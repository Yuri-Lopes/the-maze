const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let linha = [];


function Mapa() {
    for (let i = 0; i < map.length; i++) {
        linha[i] = map[i].split('');
        let divLinha = document.createElement('div');
        divLinha.id = 'linha' + i;
        divLinha.style = 'height:25px'
        let destinoLinha = document.getElementById('principal');
        destinoLinha.appendChild(divLinha);

        for (let j = 0; j < linha[i].length; j++) {
            let divWall = document.createElement('div');
            divWall.id = 'wall';

            let divfundo = document.createElement('div');
            divfundo.id = 'fundo';

            let divStart = document.createElement('div');
            divStart.id = 'start';
            
            let divFinish = document.createElement('div');
            divFinish.id = 'finish';
            if (linha[i][j] == 'W') {
                let localoz = document.getElementById(divLinha.id);
                localoz.appendChild(divWall);
            } else if (linha[i][j] == 'S') {
                let localoz = document.getElementById(divLinha.id);
                localoz.appendChild(divStart);
            } else if (linha[i][j] == 'F') {
                let localoz = document.getElementById(divLinha.id);
                localoz.appendChild(divFinish);
            } else {
                let localoz = document.getElementById(divLinha.id);
                localoz.appendChild(divfundo);
            }
        }
    }
    let player = document.createElement('div');
    player.id = 'player';
    document.getElementById('start').appendChild(player);
}

Mapa();


let x = 0;
let y = 9;

function movimentação(x, y) {
    let idLinha = 'linha' + y;
    let proxposição = document.getElementById(idLinha).children[x];
    console.log(proxposição);
    if (proxposição.id == 'fundo') {
        return true;
    } else if (proxposição.id == 'finish') {
        finish();
    } else {
        return false;
    }
}

function cadeia(x, y) {
    let idLinha = 'linha' + y;
    let proxposição = document.getElementById(idLinha).children[x];
    console.log(proxposição);
    let player = document.getElementById('player');
    proxposição.appendChild(player);
}

function finish() {
    window.alert('Parabéns, você chegou ao destino final')
    document.location.reload(true);
}
// movimentação
document.addEventListener('keydown', (e) => {
    switch (e.key) {
        case 'ArrowDown':
            if (movimentação(x, y + 1)) {
                y += 1;
            }
            break;
        case 'ArrowUp':
            if (movimentação(x, y - 1)) {
                y -= 1;
            }
            break;
        case 'ArrowLeft':
            if (movimentação(x - 1, y)) {
                x -= 1;
            }
            break;
        case 'ArrowRight':
            if (movimentação(x + 1, y)) {
                x += 1;
            }
            break;
    }
    cadeia(x, y);
});
